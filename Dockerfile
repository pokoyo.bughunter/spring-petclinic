#FROM anapsix/alpine-java
FROM maven:3.8.5-openjdk-18
LABEL maintainer="Kennedy Sanchez"
COPY /target/spring-petclinic-1.5.1.jar /home/spring-petclinic-1.5.1.jar
CMD ["java","-jar","/home/spring-petclinic-1.5.1.jar"]

#FROM jenkins/jenkins:lts-jdk11
#RUN jenkins-plugin-cli --plugins pipeline-model-definition github-branch-source:1.8
